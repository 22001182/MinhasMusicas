//
//  ViewController.swift
//  MinhasMusicas
//
//  Created by COTEMIG on 25/08/22.
//

import UIKit

struct Musica {
    let nomeMusica : String
    let nomeAlbum : String
    let nomeCantor : String
    let Imagempequena : String
    let Imagemgrande : String
}


class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var listaDeMusica:[Musica] = []
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaDeMusica.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! Mycell
        let musica = self.listaDeMusica[indexPath.row]
        
        cell.musica.text = musica.nomeMusica
        cell.album.text = musica.nomeAlbum
        cell.cantor.text = musica.nomeCantor
        cell.capa.image = UIImage(named: musica.Imagempequena)
        
        return cell
    
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)  {
        self.performSegue(withIdentifier: "abrirDetalhe", sender: indexPath.row)
       
        
    }
     
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detalhesViewController = segue.destination as! DetalheMusicaViewController
        let indice = sender as! Int
        let musica = self.listaDeMusica[indice]
        
        detalhesViewController.nomeImagem = musica.Imagemgrande
        detalhesViewController.nomeMusica = musica.nomeMusica
        detalhesViewController.nomeAlbum = musica.nomeAlbum
        detalhesViewController.nomeCantor = musica.nomeCantor
        
        
        
        
        
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.listaDeMusica.append(Musica(nomeMusica: "Ponto Cardeais", nomeAlbum: "Álbum Vivo!", nomeCantor: "Alceu Valença", Imagempequena: "capa_alceu_pequeno", Imagemgrande: "capa_alceu_grande"))
        
        self.listaDeMusica.append(Musica(nomeMusica: "Menor abondonado", nomeAlbum: "Álbum Patota de cosme", nomeCantor: "Zeca Pagodinho", Imagempequena: "capa_zeca_pequeno", Imagemgrande: "capa_zeca_grande"))
        
        self.listaDeMusica.append(Musica(nomeMusica: "Tiro ao Álvaro", nomeAlbum: "Álbum Adoniran Barbosa e Convidados", nomeCantor: "Adoniran Barbosa", Imagempequena: "capa_adoniran_pequeno", Imagemgrande: "capa_adonian_grande"))
       
    }


}

